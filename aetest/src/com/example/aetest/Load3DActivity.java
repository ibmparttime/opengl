package com.example.aetest;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;

public class Load3DActivity extends Activity
{
	private GLSurfaceView glView;
	private MyRenderer mr;

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		// 加载图片
		LoadImage.loadi(getResources());
		// 加载文件
		new LoadAssets(getResources());
		// 创建画布
		glView = new GLSurfaceView(this);
		// 创建一个渲染器MyRenderer
		mr = new MyRenderer();
		// 设置渲染器
		glView.setRenderer(mr);
		glView.setOnTouchListener(mr);
		// 显示出来
		setContentView(glView);
		Log.d("AAAA", "view created!");
	}

	@Override
	protected void onPause()
	{
		Log.d("AAAA", "view onPause!");

		super.onPause();
		glView.onPause();
	}

	@Override
	protected void onResume()
	{
		Log.d("AAAA", "view onResume!");
		super.onResume();
		glView.onResume();
	}

	@Override
	protected void onDestroy()
	{
		Log.d("AAAA", "view onDestroy!");
		super.onDestroy();
	}

}

// 载入纹理图片
class LoadImage
{
	public static Bitmap bitmap1;
	public static Bitmap bitmap2;

	public static void loadi(Resources res)
	{
		bitmap1 = BitmapFactory.decodeResource(res, R.drawable.s1);
		bitmap2 = BitmapFactory.decodeResource(res, R.drawable.s2);
	}
}

// 载入Assets文件夹下的文件
class LoadAssets
{
	public static Resources res;

	// 构造方法
	public LoadAssets(Resources resources)
	{
		res = resources;
	}

	public static InputStream loadf(String fileName)
	{
		AssetManager am = LoadAssets.res.getAssets();
		try
		{
			return am.open(fileName, AssetManager.ACCESS_UNKNOWN);
		}
		catch (IOException e)
		{
			return null;
		}

	}
}