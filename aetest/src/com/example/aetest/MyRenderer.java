package com.example.aetest;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Interact2D;
import com.threed.jpct.Loader;
import com.threed.jpct.Matrix;
import com.threed.jpct.Object3D;
import com.threed.jpct.RGBColor;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;
import com.threed.jpct.util.BitmapHelper;
import com.threed.jpct.Camera;

public class MyRenderer implements Renderer, OnTouchListener
{
	Camera Camera;

	// 初始化时的缩放系数，当输入的3D文件模型较大是将其缩放系数调小
	private float thingScale = 0.1f;

	// world对象
	private World world;

	// FrameBuffer对象
	private FrameBuffer fb;

	// Object3D
	private Object3D thing;
	private Object3D thing2;

	// 视角位置变化
	private float xpos = -1;
	private float ypos = -1;

	// 旋转度数
	private float rotateY = 0;
	private float rotateX = 0;

	// 接触标志
	boolean touchflag = false;

	boolean moveflag = false;

	boolean doublemoveflag = false;

	// 两点触控的距离
	float olddistance = 0;
	float oldx0 = 0;
	float oldy0 = 0;
	float oldx1 = 0;
	float oldy1 = 0;

	// 平移
	float translateX = 0;
	float translateY = 0;

	// 缩放
	float rateScale = 1;

	private int pointcount = 0;

	public void onDrawFrame(GL10 gl)
	{
		// 可以在这里进行变换，只有在touchflag == 1时候才调整视角
		if (touchflag)
		{
			Log.d("AAAA", "" + pointcount+"    "+rotateY);
			if (pointcount == 1)
			{

				// 旋转(1.调整camera方法 （2）调整object方法

				Camera.moveCamera(Camera.CAMERA_MOVERIGHT,
						(float) (0 + rotateX));
				Camera.moveCamera(Camera.CAMERA_MOVEDOWN, (float) (0 + rotateY));
				Camera.lookAt(thing.getTransformedCenter());

				// thing.rotateY(rotateY);
				// thing.rotateX(rotateX);
				// thing2.rotateY(rotateY);
				// thing2.rotateX(rotateX);

				// pick up
				SimpleVector dir = Interact2D.reproject2D3DWS(
						world.getCamera(), fb, (int) xpos, (int) ypos)
						.normalize();
				Object[] res = world.calcMinDistanceAndObject3D(world
						.getCamera().getPosition(), dir, 1000000000);

				if (res[1] != null)
				{
					Object3D picked = (Object3D) res[1];

					if (picked.getID() == thing.getID())
					{
						// Log.d("AAAA", "你碰到了多个小物体！");
						thing.setTexture("texture2");
						thing2.setTexture("texture2");
					}
					if (picked.getID() == thing2.getID())
					{
						// Log.d("AAAA", "你碰到了球！");
					}
				}
				else
				{
					// Log.d("AAAA", "你啥都没碰到！");
				}
			}

			else if (pointcount == 2)
			{
				// 平移
				Camera.moveCamera(Camera.CAMERA_MOVELEFT, translateX);
				Camera.moveCamera(Camera.CAMERA_MOVEDOWN, translateY);

				// thing.translate(0.0f + translateX, 0.0f + translateY, 0.0f);
				// thing2.translate(0.0f + translateX, 0.0f + translateY, 0.0f);

				// 缩放
				Camera.moveCamera(Camera.CAMERA_MOVEIN, rateScale - 1);

				// thing.scale(rateScale);
				// thing2.scale(rateScale);

			}

			// 回归初始
			translateX = 0;
			translateY = 0;
			rotateX = 0;
			rotateY = 0;
			rateScale = 1;
		}
		else
		{
			thing.setTexture("texture");
			thing2.setTexture("texture");

		}

		// 以黑色清除整个屏幕
		fb.clear(RGBColor.BLACK);
		// 对所有多边形进行变换及灯光操作
		world.renderScene(fb);
		// 绘制已经由renderScene产生的fb
		world.draw(fb);
		// 渲染显示图像
		fb.display();
	}

	public void onSurfaceChanged(GL10 gl, int width, int height)
	{
		if (fb != null)
		{
			fb = null;
		}
		fb = new FrameBuffer(gl, width, height);
	}

	public void onSurfaceCreated(GL10 gl, EGLConfig arg1)
	{
		world = new World();
		// 设置环境光
		world.setAmbientLight(150, 150, 150);

		// 循环将已存在Texture以新的名字存入TextureManager中

		/*
		 * for (int i = 0; i < textures.length; i++) {
		 * TextureManager.getInstance().addTexture(textures[0], new
		 * Texture(LoadImage.bitmap)); }
		 */

		Texture texture1 = new Texture(BitmapHelper.rescale(LoadImage.bitmap1,
				512, 512));

		Texture texture2 = new Texture(BitmapHelper.rescale(LoadImage.bitmap2,
				512, 512));

		try
		{
			TextureManager.getInstance().addTexture("texture", texture1);
			TextureManager.getInstance().addTexture("texture2", texture2);
		}
		catch (Exception e)
		{
			Log.d("AAAA", "texture have already been there!");
		}

		// 加载模型
		thing = loadModel("dog.obj", thingScale);
		thing2 = loadModel("0999.obj", thingScale);

		// 为Object3D对象设置纹理
		// thing.setTexture("texture");
		// 设置碰撞
		thing.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);

		thing2.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);

		Log.d("AAAA", "" + thing.getID());
		Log.d("AAAA", "" + thing2.getID());

		// 渲染绘制前进行的操作
		thing.build();
		thing2.build();
		world.addObjects(thing, thing2);
		// 将thing添加Object3D对象中

		// 调整坐标系
		Camera = world.getCamera();
		Camera.setPosition(5, 0, -5);
		// 在world中应用刚设置好了的坐标系
		world.getCamera().lookAt(thing.getTransformedCenter());
	}

	// 载入模型
	private Object3D loadModel(String filename1, float scale)
	{
		// 将载入的3ds文件保存到model数组中

		// Object3D[] model = Loader.load3DS(LoadAssets.loadf(filename1),
		// scale);
		Object3D[] model = Loader.loadOBJ(LoadAssets.loadf(filename1), null,
				scale);

		// LoadAssets.loadf(filename2), scale);

		// 取第一个3ds文件
		Object3D o3d = new Object3D(0);
		// 临时变量temp
		Object3D temp = null;
		// 遍历model数组
		for (int i = 0; i < model.length; i++)
		{
			// 给temp赋予model数组中的某一个
			temp = model[i];
			// 设置temp的中心为 origin (0,0,0)
			temp.setCenter(SimpleVector.ORIGIN);
			// 沿x轴旋转坐标系到正常的坐标系(jpct-ae的坐标中的y,x是反的)
			temp.rotateX((float) (-.5 * Math.PI));
			// 使用旋转矩阵指定此对象旋转网格的原始数据
			temp.rotateMesh();
			// new 一个矩阵来作为旋转矩阵
			temp.setRotationMatrix(new Matrix());
			// 合并o3d与temp
			o3d = Object3D.mergeObjects(o3d, temp);
			// 主要是为了从桌面版JPCT到android版的移徝(桌面版此处为o3d.build())
			o3d.compile();
		}
		// 返回o3d对象
		return o3d;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		pointcount = event.getPointerCount();
		float x0 = -1;
		float y0 = -1;
		float x1 = -1;
		float y1 = -1;
		// 一个手放上时拖动为旋转物体
		if (pointcount == 1)
		{
			if (event.getAction() == MotionEvent.ACTION_DOWN)
			{
				xpos = event.getX();
				ypos = event.getY();
				touchflag = true;
				return true;
			}
			if (event.getAction() == MotionEvent.ACTION_MOVE)
			{
				moveflag = true;
				float xd = event.getX() - xpos;
				float yd = event.getY() - ypos;
				// Log.d("AAAA", "" + xd);

				if (xd > 100)
				{
					rotateX = (float) -0.3;
				}
				if (xd < -100)
				{
					rotateX = (float) +0.3;
				}
				if (yd > 100)
				{
					rotateY = (float) -0.3;
				}
				if (yd < -100)
				{
					rotateY = (float) +0.3;
				}
				touchflag = true;
				return true;
			}
			if (event.getAction() == MotionEvent.ACTION_UP)
			{
				xpos = -1;
				ypos = -1;

				touchflag = false;
				return true;
			}
		}
		if (pointcount == 2)
		{
			if (event.getAction() == MotionEvent.ACTION_DOWN)
			{
				touchflag = true;
				return true;
			}
			if (event.getAction() == MotionEvent.ACTION_UP)
			{
				x0 = -1;
				touchflag = false;
				return true;
			}
			if (event.getAction() == MotionEvent.ACTION_MOVE)
			{
				x0 = event.getX(0);
				y0 = event.getY(0);
				x1 = event.getX(1);
				y1 = event.getY(1);
				float distance = (x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1);
				float change = distance - olddistance;
				if (change > 2000)
				{
					rateScale = rateScale + 0.1f;
				}
				else if (change < -2000)
				{
					rateScale = rateScale - 0.1f;
				}
				else
				{
					float kk = x0 - oldx0;
					if (x0 - oldx0 > 4 && x1 - oldx1 > 4)
					{
						translateX = 0.15f;
					}
					if (x0 - oldx0 < -4 && x1 - oldx1 < -4)
					{
						translateX = -0.15f;
					}
					if (y0 - oldy0 > 4 && y1 - oldy1 > 4)
					{
						translateY = 0.15f;
					}
					if (y0 - oldy0 < -4 && y1 - oldy1 < -4)
					{
						translateY = -0.15f;
					}

				}
				olddistance = distance;
				oldx0 = x0;
				oldy0 = y0;
				oldx1 = x1;
				oldy1 = y1;
			}

		}
		return true;
	}
}
